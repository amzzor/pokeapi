// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// import { getAnalytics } from "firebase/analytics";
import { getFirestore } from 'firebase/firestore/lite';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDA0whfbPYsJrVnRHcAsGy_SsY8eoxQ8D8",
  authDomain: "puregrading-3d20c.firebaseapp.com",
  projectId: "puregrading-3d20c",
  storageBucket: "puregrading-3d20c.appspot.com",
  messagingSenderId: "795926199163",
  appId: "1:795926199163:web:a0b90afe7b2f1fdda61bde",
  measurementId: "G-YFEEPMHV6P"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
// const analytics = getAnalytics(app);

const db = getFirestore(app);

export { db };