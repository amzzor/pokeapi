import { db } from "../config/fbConfig";
import { collection, doc, setDoc, addDoc, getDocs, query, where } from "firebase/firestore/lite"; 

export async function addSets(sets){
  try {
    sets.forEach(set => {
      var id = Math.random().toString(36).substr(2, 9);
      setDoc(doc(db, "sets", id), set);
    });
  } catch (error) {
    console.log(error);
    return {
      code: 400,
      error: error
    };
  }
}

export async function addAllSeries(){
  try {
    let series = [];
    let dataURL = "https://api.tcgdex.net/v2/fr/series";
    fetch(dataURL)
    .then(res => res.json())
    .then(res => {
        series = res;
        series.forEach(serie => {
          var id = Math.random().toString(36).substr(2, 9);
          setDoc(doc(db, "series", id), serie);
        });
    })
    
  } catch (error) {
    console.log(error);
    return {
      code: 400,
      error: error
    };
  }
}

export async function addSetToSerie(set){
  try {
    console.log(set)
    const serieRef = collection(db, "series");
    
      const q = query(serieRef, where("id", "==", set.serie.id));
      const querySnapshot = await getDocs(q);
      querySnapshot.forEach((doc) => {
        // doc.data() is never undefined for query doc snapshots
        console.log(doc.id, " => ", doc.data());
        addDoc(collection(db, "series", doc.id, "sets"), set)
      });
    
    // var id = Math.random().toString(36).substr(2, 9);
    // q.setDoc(doc(db, "series", id), card);
    
  } catch (error) {
    console.log(error);
    return {
      code: 400,
      error: error
    };
  }
}

export async function retrieveSets(){
  try {
    let sets = [];

    const querySnapshot = await getDocs(collection(db, "sets"));
    querySnapshot.forEach((doc) => {
      // doc.data() is never undefined for query doc snapshots
      // console.log(doc.id, " => ", doc.data());
      sets.push({id: doc.id, data: doc.data()})
    });

    return sets;
  } catch (error) {
    console.log(error);
    return {
      code: 400,
      error: error
    };
  }
}

export async function retrieveSetsFromSeries(id, data){
  try {
    let sets = [];
    const querySnapshot = await getDocs(collection(db, "series", id, "sets"));
    for (let i = 0; i < querySnapshot.size; i++) {
      const doc = querySnapshot.docs[i];
      const setData = doc.data();
      const setId = doc.id;
      sets.push({id: setId, data: setData})
    }
    return sets;
  } catch (error) {
    console.log(error);
    return {
      code: 400,
      error: error
    };
  }
}

export async function SeriesTransform(querySnapshot){
  const series = [];
  for (let i = 0; i < querySnapshot.size; i++) {
    const doc = querySnapshot.docs[i];
    const data = doc.data();
    const id = doc.id;
    const result = await retrieveSetsFromSeries(id, data);
    series.push({id: doc.id, data: doc.data(), sets: result});
  }
  return series;
}

export async function retrieveSeriesWithSets(){
  try {
    const querySnapshot = await getDocs(collection(db, "series"));

    const series = await SeriesTransform(querySnapshot);
    // querySnapshot.forEach((doc) => {
    //   // doc.data() is never undefined for query doc snapshots
    //   // console.log(doc.id, " => ", doc.data());
      
    //   const result = retrieveSetsFromSeries(doc.id, doc.data())
    //   // const querySnapshot = getDocs(q);
    //   // querySnapshot.forEach((document) => {
    //   //   // doc.data() is never undefined for query doc snapshots
    //   //   console.log(document.id, " => ", document.data());

    //   //   series.push({id: doc.id, data: doc.data(), sets: {id: document.id, data: document.data()}})
    //   // });

    //   series.push(result)

    // });

    // console.log(series)
    return series;
  } catch (error) {
    console.log(error);
    return {
      code: 400,
      error: error
    };
  }
}

export async function retrieveSetById(id){
  let result = undefined;
  let dataURL = "https://api.tcgdex.net/v2/fr/sets/"+id;
  await fetch(dataURL)
  .then(res => res.json())
  .then(res => {
    result = res;
  })

  return result;
}

export async function retrieveCardsBySets(sets){
  try {
    const cards = [];
    for (let i = 0; i < sets.length; i++) {
      let res = await retrieveSetById(sets[i].data.id);
      const serieId = res.serie.id;
      const serieName = res.serie.name;
      // res.serie.id !== undefined ? cardInfos.serieId = res.serie.id : cardInfos.serieId = null;
      // res.serie.namae !== undefined ? cardInfos.serieName = res.serie.name : cardInfos.serieName = null;
      res.cards.forEach(card => {
        let cardInfos = {
          id: null,
          localId: null,
          name: null,
          setId: sets[i].data.id,
          setName: sets[i].data.name,
          serieId: serieId,
          serieName: serieName,
          image: null,
          releaseDate: null
        }
        cardInfos.releaseDate = res.releaseDate;
        cardInfos.id = card.id;
        cardInfos.localId = card.localId;
        if(card.image !== undefined){
          cardInfos.image = card.image;
        }
        cardInfos.name = card.name;
        cards.push(cardInfos);
      })
    }

    return cards;
  } catch (error) {
    console.log(error);
    return {
      code: 400,
      error: error
    };
  }
}

export async function retrieveAllSeries(){
  try {
    const series = [];
    const querySnapshot = await getDocs(collection(db, "series"));
    querySnapshot.forEach((doc) => {
      // doc.data() is never undefined for query doc snapshots
      // console.log(doc.id, " => ", doc.data());
      series.push({id: doc.id, data: doc.data()})
    });
    return series;
  } catch (error) {
    console.log(error);
    return {
      code: 400,
      error: error
    };
  }
}

export async function retrieveAllCards(){
  try {
    const cards = [];
    // const sets = await retrieveSets();
    // const cards = await retrieveCardsBySets(sets);
    const querySnapshot = await getDocs(collection(db, "cards"));
    querySnapshot.forEach((doc) => {
      // doc.data() is never undefined for query doc snapshots
      // console.log(doc.id, " => ", doc.data());
      cards.push({id: doc.id, data: doc.data()})
    });
    return cards;
  } catch (error) {
    console.log(error);
    return {
      code: 400,
      error: error
    };
  }
}

export async function addAllCards(cards){
  try {
    cards.forEach(card => {
      var id = Math.random().toString(36).substr(2, 9);
      setDoc(doc(db, "cards", id), card);
    });
  } catch (error) {
    console.log(error);
    return {
      code: 400,
      error: error
    };
  }
}