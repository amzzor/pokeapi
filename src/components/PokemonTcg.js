import { useState ,useEffect } from 'react';

import '../App.css';

export default function JpnCards() {
    const [cards, setCards] = useState(undefined);

    useEffect(() => {
        function fetchData(){
            let dataURL = "https://www.jpn-cards.com/set";
            fetch(dataURL)
            .then(res => res.json())
            .then(res => {
                setCards(res);
            })
        }

        fetchData();
    }, []);

    console.log(cards)

    if(cards === undefined){
        return "";
    }

    return (
        <div className="App">
            <h1>jpn-cards :</h1>
            {/* {cards.slice(0,4).map(card => {
                return(
                <div className="infos">
                    <p>id : {card.id}</p>
                    <p>Nom de la carte : {card.name}</p>
                    <p>Année : {card.year}</p>
                    <img src={card.setUrl} alt={card.name} />
                </div>
                );
            })} */}
        </div>
    );
}