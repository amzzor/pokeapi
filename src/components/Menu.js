import { Link } from "react-router-dom";

export default function Menu() {
    return (
        <nav>
            <ul>
                <li><Link to="/tcgdex">TCGdex</Link></li>
                <li><Link to="/jpn-cards">jpn-cards</Link></li>
            </ul>
        </nav>
    );
}
  
  