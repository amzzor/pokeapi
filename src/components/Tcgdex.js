import { useState , useEffect } from 'react';
import {  retrieveSeriesWithSets } from '../services/FirebaseService';

import '../App.css';

function _card(card){
    console.log(card)
    return(
    <div className="infos">
        <p>id : {card.id}</p>
        <p>Nom de la carte : {card.name}</p>
        <img src={card.image} alt={card.name} />
    </div>
    );
}

function _cards(cards){
    return cards.map(card => _card(card))
}

export default function Tcgdex() {
    // const [cards, setCards] = useState(undefined);
    // const [sets, setSets] = useState(undefined);
    const [series, setSeries] = useState(undefined);
    const [selectedSerie, setSelectedSerie] = useState(undefined);
    const [selectedSet, setSelectedSet] = useState(undefined);
    const [selectedCards, setSelectedCards] = useState(undefined);

    useEffect(() => {
        // function fetchData(){
        //     // let dataURL = "https://api.tcgdex.net/v2/fr/cards";
        //     let dataURL = "https://api.tcgdex.net/v2/fr/sets/bw10";
        //     fetch(dataURL)
        //     .then(res => res.json())
        //     .then(res => {
        //         console.log(res)
        //         setCards(res);
        //     })
        // }

        // function fetchSets(){
        //     let dataURL = "https://api.tcgdex.net/v2/fr/sets";
        //     fetch(dataURL)
        //     .then(res => res.json())
        //     .then(res => {
        //         setSets(res);
        //     })
        // }
        // async function getAllSets(){
        //     const seriesData = await retrieveAllSeries();
        //     setSeries(seriesData);
        //     const setsData = await retrieveSets();
        //     setSets(setsData);
        //     setsData.forEach(set => {
        //         let dataURL = "https://api.tcgdex.net/v2/fr/sets/"+set.data.id;
        //         fetch(dataURL)
        //         .then(res => res.json())
        //         .then(res => {

        //             // addSetToSerie(res);
        //             // setCards(res);
        //         })
                
        //     })

        //     //console.log(set.data)

        // }

        async function getSeriesWithSets(){
            const s = await retrieveSeriesWithSets();
            setSeries(s);
        }
        // fetchData();
        // fetchSets();
        // getAllSets();
        // const result = retrieveAllCards();
        // setCards(result)
        getSeriesWithSets();
        
    }, []);


    if(series === undefined){
        return "";
    }

    // if(cards !== undefined){
    //     console.log(cards.message)
    // }

    
    console.log(selectedCards);

    return (
        <>
            <nav>
            <ul>
                {series.map(serie => {
                return(
                <li key={serie.id} onClick={() => setSelectedSerie(serie.id)}>
                    {serie.data.name}
                    {selectedSerie === serie.id ?
                    <ul>
                        {serie.sets.map(set => {
                        return(
                        <li key={set.id} onClick={() => {
                            setSelectedSet(set.id);
                            setSelectedCards(set.data.cards);
                        }}>{set.data.name}</li>
                        )
                        })}
                    </ul>
                    :
                    ""
                    }   
                </li>
                
                )
                })}
            </ul>
            </nav>
        <div className="App">
            <h1>TCGdex :</h1>
            {selectedCards !== undefined && selectedSet !== undefined ? _cards(selectedCards) : ""}
            {/* {cards.map(card => {
                return(
                <div className="infos">
                    <p>id : {card.id}</p>
                    <p>Nom de la carte : {card.name}</p>
                    <img src={card.image} alt={card.name} />
                </div>
                );
            })} */}
                {/* <div className="infos">
                    <p>id : {cards.id}</p>
                    <p>Nom de la carte : {cards.name}</p>
                    <p>Set : {cards.set.name}</p>
                    <img src={cards.image} alt={cards.name} />
                </div> */}
        </div>
    </>
    );
}