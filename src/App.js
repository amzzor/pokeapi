import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

import Tcgdex from "./components/Tcgdex";
import JpnCards from "./components/JpnCards";
import Menu from "./components/Menu";
import './App.css';

function App() {
  return (
      <Router>
        <Menu />
        <Routes>
          <Route path="/tcgdex" element={<Tcgdex />} />
          <Route path="/jpn-cards" element={<JpnCards />} />
        </Routes>
      </Router>
  );
}

export default App;
